#
# Be sure to run `pod lib lint VidyoIOClient.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|

s.name             = 'VidyoIOClient'
s.version          = '19.3.1.6'
s.summary          = 'Vidyo.io iOS client library.'

s.description      = <<-DESC
Vidyo.io™ is a platform as a service (PaaS) that enables developers to provide high-quality, real-time video communication capabilities. The service includes the VidyoClient SDK, which provides the APIs for integrating such communication capabilities into a variety of applications and workflows.
DESC

s.homepage         = 'https://bitbucket.org/tmelko-gl-vidyo/vidyoclient-ios'

s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'Taras' => 'taras.melko@globallogic.com' }
s.source           = { :git => 'https://tmelko-gl-vidyo@bitbucket.org/tmelko-gl-vidyo/vidyoclient-ios.git', :tag => '19.3.1.6' }

s.ios.deployment_target = '8.0'
  
# Header search path setup
s.xcconfig = { 'HEADER_SEARCH_PATHS' => '${PODS_ROOT}/VidyoIOClient/VidyoClientIOS.framework/Headers' }

# Dependency frameworks
s.frameworks = 'AudioToolbox', 'AVFoundation', 'CoreLocation', 'CoreMedia', 'SystemConfiguration', 'UIKit'

# Vidyo.io framework
s.vendored_frameworks = "VidyoClientIOS.framework"

s.requires_arc = false

end
