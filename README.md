Vidyo.io Client for iOS
===================

VidyoClient is a library packaged as a Pod for integration in iOS apps that require access to Vidyo.io services. For VidyoClient API documentation check out the following url: 

https://developer.vidyo.io/#/documentation

## How to Use

``$ pod init``

To use this pod in your iOS app, add the following to your **Podfile**:

Specify the source URL of the spec repo at the top of the pod file:

``source 'https://tmelko-gl-vidyo@bitbucket.org/tmelko-gl-vidyo/vidyoclient-ios-spec.git'``

cocoapods knows where to find the installation of your pod

Add to the target scopes:

``pod 'VidyoIOClient'``

or

``pod 'VidyoIOClient', '~> 4.1.25.46'``

with specified version to avoid use latest one.

**Podfile** sample:

> \# Uncomment the next line to define a global platform for your project
> \# platform :ios, '9.0'
>  source 'https://tmelko-gl-vidyo@bitbucket.org/tmelko-gl-vidyo/vidyoclient-ios-spec.git'
> target 'YourProjectName' do
> \# Uncomment the next line if you're using Swift or would like to use dynamic frameworks
>\# use_frameworks!
> pod 'VidyoIOClient'
> end

## Contents of this repo

- ```VidyoClientIOS.framework``` - an iOS frameworks
- ```VidyoIOClient.podspec``` - the podspec file

## Maintenance

When Vidyo issues a new VidyoClient library, the following procedure should be followed to update the podspec: 

#### 1. Add your **VidyoIOClient** repo to your CocoaPods installation 
This step should only be performed once on your machine. 

``$ pod repo add VidyoIOClient https://tmelko-gl-vidyo@bitbucket.org/tmelko-gl-vidyo/vidyoclient-ios-spec.git``

#### 2. Update VidyoIOClient.podspec

- Overwrite VidyoClientIOS.framework with the new library provided by Vidyo.io (keep the same name: **VidyoClientIOS.framework**)
- Revise VidyoIOClient.podspec by updating ```s.version``` and "tag:" inside ```s.source```. The value should be the new tag.
- ```git commit -am 'comment'``` and ```git push```
- ```git tag <new_tag>``` and ```git push --tags```
- Use pod spec lint to test your changes: 

``$ pod lib lint VidyoIOClient.podspec --verbose --allow-warnings --skip-import-validation``

- Push the changes and tag the commit with the same tag used for ```s.version``` and ```s.source``` in the podspec.

#### 3. Push the new version of VidyoIOClient.podspec to VidyoIOClient repo

``$ pod repo push VidyoIOClient VidyoIOClient.podspec --verbose --skip-import-validation``
